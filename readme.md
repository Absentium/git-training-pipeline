
### Basisopdracht
sidenote; Gebruik van een taalmodel(zoals chatgpt of gemini) is voor het opzetten van pipelines, of het in kleine stukken breken voor uitleg een erg goede tool. Probeer bij de basis opdracht hier nog géén gebruik van te maken.
1.  **Fork de repository**: Ga naar de GitLab repository die we voor je hebben voorbereid. Maak een fork van deze repository naar je eigen GitLab-account.
    
2.  **Clone de geforkte repository**: Clone de geforkte repository naar je lokale machine, zodat je er wijzigingen in kunt aanbrengen.
    
3.  **Bekijk de Java testbestanden**: Navigeer naar de map `tests` in je lokale kopie van de repository. Bekijk de Java testbestanden om te begrijpen wat ze doen.
    
4.  **Creëer een `.gitlab-ci.yml` bestand**: Maak in de root van je project een nieuw bestand met de naam `.gitlab-ci.yml`. Dit YAML-bestand configureert je GitLab CI/CD pipeline.
    
5.  **Configureer je pipeline**:
    
    -   Voeg in het `.gitlab-ci.yml` bestand een stage toe genaamd `test`.
    -   Definieer een job binnen deze stage die de Java tests uitvoert. Gebruik een passend Docker image voor Java, zoals `openjdk:11`.
6.  **Commit en push je wijzigingen**: Voeg het `.gitlab-ci.yml` bestand toe aan je repository, commit de wijziging met een beschrijvende boodschap, en push deze naar je GitLab repository.
    
7.  **Bekijk de pipeline uitvoering**: Ga naar je GitLab repository en bekijk de CI/CD sectie om te zien hoe je pipeline automatisch wordt uitgevoerd en de Java tests draait.

### Extra uitdaging

1.   **Optimaliseer de pipeline**: Experimenteer met verschillende configuraties in je `.gitlab-ci.yml` bestand om de uitvoeringstijd van je pipeline te verminderen.
    -   Voeg caching toe aan je pipeline om de bouwtijd te verminderen. Caching slaat bepaalde bestanden op tussen runs, zoals dependencies, zodat ze niet bij elke build opnieuw gedownload hoeven te worden.

2.  **Conditionele Uitvoering:**
    
    -   Configureer de pipeline om alleen uit te voeren bij wijzigingen in specifieke mappen of bestanden. Dit voorkomt onnodige builds als er geen relevante wijzigingen zijn.

3.   **Voeg meer tests toe**: Schrijf en voeg extra Java testcases toe aan de `tests` map. Zorg ervoor dat je pipeline deze ook uitvoert.

4.   **Voeg een notificatie toe**: Configureer je pipeline zodanig dat je een notificatie ontvangt (bijvoorbeeld via e-mail of Slack) als de tests falen.
### Codekwaliteit en -stijlcontrole

1.  **Statische Code-analyse:**
    
    -   Integreer tools zoals SonarQube of Checkstyle in je pipeline voor statische code-analyse. Deze tools controleren je code op complexiteit, bugs, stijlfouten en meer, om de kwaliteit te waarborgen. SonarQube is gratis beschikbaar zo lang als 
2.  **Formatteringscontroles:**
    
    -   Gebruik tools zoals Spotless om de codeconsistentie te waarborgen. Deze tools zorgen ervoor dat je code voldoet aan bepaalde formatteerregels en standaarden.

### Integratie met Externe Services

1.  **Notificaties:**
    -   Integreer je pipeline met externe services zoals Slack of e-mail om notificaties te ontvangen over de status van je builds. Dit helpt je team op de hoogte te blijven van de voortgang en eventuele problemen snel aan te pakken.

2.  **Artefacten Opslaan:**
    -   Configureer de pipeline om artefacten te uploaden naar een opslagplaats. Dit kan GitLab's eigen artifact repository zijn of een externe service zoals AWS S3. Artefacten kunnen gecompileerde code, testrapporten of andere bestanden zijn die je wilt bewaren na een build.

### Parameterisatie en Dynamische Configuratie
-   Maak gebruik van variabelen en parameters in je `.gitlab-ci.yml` bestand om de configuratie van je pipeline dynamisch te maken. Hierdoor kun je gemakkelijk de Java-versie aanpassen of andere configuratie-opties wijzigen zonder de pipeline handmatig te moeten bewerken.


### Toepassen van GitLab CI-technieken

-   Maak gebruik van 'only' en 'except' keywords in je pipeline configuratie om te bepalen onder welke omstandigheden bepaalde jobs moeten worden uitgevoerd. Dit kan gebaseerd zijn op branches, tags, of wijzigingen in specifieke bestanden.

### Falen en Herstellen

-   Voeg bewust een fout toe aan een van je tests om te zien hoe de pipeline reageert op falende tests. Dit helpt je te begrijpen hoe je snel problemen kunt identificeren en oplossen.

Door deze stappen te volgen, bouw je een robuuste CI/CD-pipeline die je Java-applicatie bouwt, test, en de kwaliteit bewaakt. Dit proces automatiseert veel van de routinechecks en -taken, waardoor je team efficiënter kan werken en zich kan concentreren op het verbeteren van de applicatie zelf.