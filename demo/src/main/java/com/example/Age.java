package com.example;

import org.joda.time.LocalDate;
import org.joda.time.Years;

public class Age 
{
    public static int calculateAge(LocalDate birthDate) {
        LocalDate now = new LocalDate();
        Years age = Years.yearsBetween(birthDate, now);
        return age.getYears();
    }

    public static boolean isAdult(LocalDate birthDate) {
        return calculateAge(birthDate) >= 18;
    }
}
