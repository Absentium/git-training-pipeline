# GitLab CI/CD Cheatsheet voor Java Testen

## Basisstructuur `gitlab-ci.yml`

```yaml
stages:
  - build
  - test

build_job:
  stage: build
  image: maven:3.6.3-jdk-11
  script:
    - mvn compile

test_job:
  stage: test
  image: maven:3.6.3-jdk-11
  script:
    - mvn test
```

## Sleutelcomponenten
**stages:** Definieert de fasen van je pipeline. In dit voorbeeld zijn er twee fasen: build en test.\
**build_job:** Een job die het bouwproces van je Java applicatie uitvoert. We gebruiken Maven en JDK 11.\
**test_job:** Een job specifiek voor het uitvoeren van je Java tests met Maven.
Belangrijke Commando's en Opties.\
**image:** Specificeert het Docker image waarin de job draait. Voor Java/Maven projecten wordt vaak een Maven image met de gewenste JDK versie gebruikt.
script: Een lijst met commando's die worden uitgevoerd binnen een job. Voor Java projecten typisch mvn compile voor het bouwen en mvn test voor het testen.

## Links voor intressante pipelines
https://github.com/jnjam6681/gitlab-ci-cheat-sheet/blob/master/artifact/.gitlab-ci.yml
https://www.servicenow.com/nl/products/devops/what-is-cicd-pipeline.html
